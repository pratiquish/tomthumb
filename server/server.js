Meteor.startup(function() {
  console.log("getpagedata called");
  var data = JSON.parse(Assets.getText('thumbscraper.json'));
  //for some dependency issues I couldnt use npm packages on client side
  var xpath = Meteor.npmRequire('xpath');
  var dom = Meteor.npmRequire('xmldom').DOMParser;
  for(var page in data){
    var xml = data[page].xpath_test_query;
    var doc = new dom().parseFromString(xml);
    var node = xpath.select("a//text()", doc).value;
    console.log(node);
  }
});
// if (Meteor.isServer) {
// Meteor.methods({
//   'getPageData': function(){
//     console.log("getpagedata called");
//     var data = JSON.parse(Assets.getText('thumbscraper.json'));
//     var xpath = Meteor.npmRequire('xpath');
//     var dom = Meteor.npmRequire('xmldom').DOMParser;
//     for(var page in data){
//       var xml = data[page].xpath_button_to_click;
//       var doc = new dom().parseFromString(xml);
//       var node = xpath.select1(xml, doc).value;
//       console.log(node);
//     }
//   }
// });
// }
